package com.example.cse438.studio1.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cse438.studio1.R

@SuppressLint("ValidFragment")
class HomeFragment(context: Context): Fragment() {
    private var parentContext: Context = context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // TODO: implement this function to inflate the fragment_home.xml file for the container; don't attach to root
        return inflater.inflate(R.layout.fragment_result_list, container, false)
   }
}